﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TBZ.Interfaces;
using TBZ.Repository.Repositories;
using TBZCurso;
using TBZRepository;

namespace SISGESATBZ1.Controllers
{
    public class EstudiantesController : Controller
    {
        //
        // GET: /Estudiantes/

        private IEstudiante iestudiante;

        public EstudiantesController() { }



        public EstudiantesController(IEstudiante iestudiante)
        {
            this.iestudiante = iestudiante;
        }

        [HttpGet]
        public ViewResult GestionarEstudiante(string query = "")
        {
            var datos = iestudiante.ByQueryAll(query);
            return View("GestionarEstudiante", datos);
        }

        [HttpGet]
        public ViewResult CrearEstudiante()
        {
            return View("CrearEstudiante");
        }

        [HttpPost]
        public ActionResult CrearEstudiante(Estudiante estudiante)
        {
            EjecutarValidaciones(estudiante);
            if (ModelState.IsValid)
            {
                
                iestudiante.AddEstudiante(estudiante);
                TempData["UpdateSuccess"] = "Se Guardó Correctamente";

                return RedirectToAction("GestionarEstudiante");
            }
            return View("CrearEstudiante", estudiante);
        }

        [HttpGet]
        public ViewResult ActualizarEstudiante(int idEstudiante)
        {
            var data = iestudiante.FindEstudiante(idEstudiante);
            return View("ActualizarEstudiante", data);
        }

        [HttpPost]
        public ActionResult ActualizarEstudiante(Estudiante estudiante)
        {
            EjecutarValidaciones(estudiante);
            if (ModelState.IsValid)
            {
                iestudiante.UpdateEstudiante(estudiante);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("GestionarEstudiante");
            }
            var data = iestudiante.FindEstudiante(estudiante.IdEstudiante);
            return View("ActualizarEstudiante", data);
        }

        [HttpGet]
        public ViewResult DetalleEstudiante(int idEstudiante)
        {
            var data = iestudiante.FindEstudiante(idEstudiante);
            return View("DetalleEstudiante", data);
        }

        //Validaciones
        private void EjecutarValidaciones(Estudiante estudiante)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");

            if (String.IsNullOrEmpty(estudiante.DniEstudiante))
                ModelState.AddModelError("DniEstudiante", "Ingrese DNI");
            else
            {
                if (!regNumeros.IsMatch(estudiante.DniEstudiante))
                    ModelState.AddModelError("DniEstudiante", "El DNI debe contener solo digitos");
                else
                {
                    if (estudiante.DniEstudiante.Length != 8)
                        ModelState.AddModelError("DniEstudiante", "El DNI debe contar con 8 digitos");
                    else
                    {
                        if (!DniExiste(estudiante.DniEstudiante, estudiante.IdEstudiante))
                            ModelState.AddModelError("DniEstudiante", "El DNI Ya existe");
                    }
                }
            }

            if (String.IsNullOrEmpty(estudiante.Nombres))
                ModelState.AddModelError("Nombres", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(estudiante.Nombres))
                    ModelState.AddModelError("Nombres", "El Nombre debe contener solo Letras");
                else
                {
                    if (estudiante.Nombres.Length >= 50)
                        ModelState.AddModelError("Nombres", "El Nombre debe tener hasta 50 letras");
                }
            }
            if (String.IsNullOrEmpty(estudiante.ApellidoPaterno))
                ModelState.AddModelError("ApellidoPaterno", "Ingrese Apellido Paterno");
            else
            {
                if (!regLetras.IsMatch(estudiante.ApellidoPaterno))
                    ModelState.AddModelError("ApellidoPaterno", "El Apellido debe contener solo Letras");
                else
                {
                    if (estudiante.ApellidoPaterno.Length >= 50)
                        ModelState.AddModelError("ApellidoPaterno", "El Apellido debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(estudiante.ApellidoMaterno))
                ModelState.AddModelError("ApellidoMaterno", "Ingrese Apellido Materno");
            else
            {
                if (!regLetras.IsMatch(estudiante.ApellidoMaterno))
                    ModelState.AddModelError("ApellidoMaterno", "El Apellido debe contener solo Letras");
                else
                {
                    if (estudiante.ApellidoMaterno.Length >= 50)
                        ModelState.AddModelError("ApellidoMaterno", "El Apellido debe tener hasta 50 letras");
                }
            }

            if (String.IsNullOrEmpty(estudiante.Sexo))
                ModelState.AddModelError("Sexo", "Seleccione Sexo");

            if (String.IsNullOrEmpty(estudiante.Direccion))
                ModelState.AddModelError("Direccion", "Ingrese Direccion");

            if (String.IsNullOrEmpty(estudiante.Telefono))
                ModelState.AddModelError("Telefono", "Ingrese Teléfono");
            else
            {
                if (!regNumeros.IsMatch(estudiante.Telefono))
                {
                    ModelState.AddModelError("Telefono", "El Telefono debe contener solo digitos");
                }
                else
                {
                    if (estudiante.Telefono.Length > 10)
                        ModelState.AddModelError("Telefono", "El Telefono debe tener hasta 9 digitos");
                }
            }
            
            if (String.IsNullOrEmpty(estudiante.FechaNacimiento.ToString()))
                ModelState.AddModelError("FechaNacimiento", "Seleccione Fecha Nacimiento");
            else 
            {
                int añoActual = DateTime.Now.Year;
                int mesActual = DateTime.Now.Month;
                int diaActual = DateTime.Now.Day;
                if (estudiante.FechaNacimiento.Year >= añoActual & estudiante.FechaNacimiento.Month >= mesActual & estudiante.FechaNacimiento.Day >= diaActual)
                    ModelState.AddModelError("FechaNacimiento", "El Estudiante no puede haber nacido después o en la fecha actual: " + diaActual + "/" + mesActual + "/" + añoActual);
                else
                {
                    int añoNoPermitido = DateTime.Now.Year - 5;
                    if(estudiante.FechaNacimiento.Year >= añoNoPermitido)
                        ModelState.AddModelError("FechaNacimiento", "El Estudiante no puede tener  menos de 6 años");
                }
               
            }

            //if (String.IsNullOrEmpty(estudiante.DniApoderado.ToString()))
            //    ModelState.AddModelError("DniApoderado", "Ingrese DNI Apoderado");
            //else
            //{
            //    if (!regNumeros.IsMatch(estudiante.DniApoderado.ToString()))
            //        ModelState.AddModelError("DniApoderado", "El DNI debe contener solo digitos");
            //    else
            //    {
            //        if ((estudiante.DniApoderado.ToString()).Length != 8)
            //            ModelState.AddModelError("DniApoderado", "El DNI debe contar con 8 digitos");
            //        else
            //        {
            //            if (!DniExisteApoderado(estudiante.DniApoderado.ToString()))
            //                ModelState.AddModelError("DniApoderado", "El DNI no existe");
            //        }
            //    }
            //}
            
        }
        private bool DniExiste(string dniestudiante, int idestudiante)
        {
            var repo = new EstudianteR(new TBZContext());
            if (repo.DniExiste(dniestudiante, idestudiante) > 0)
                return false;
            return true;
        }

        //private bool DniExisteApoderado(string dniApoderado)
        //{
        //    var repo = new ApoderadoR(new TBZContext());
        //    if (repo.DniExisteApoderado(dniApoderado) > 0)
        //        return false;
        //    return true;
        //}
    }
}
