﻿using TBZCurso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TBZInterfaces;
using SISGESATBZ1;
using TBZRepository.Repositories;
using TBZRepository;

namespace SISGESATBZ1.Controllers
{
    public class CursosController :Controller
    {
        //
        // GET: /Cursos/

        private ICurso icurso;

        public CursosController(){}

        public CursosController(ICurso icurso) 
        {
            this.icurso = icurso;
        }

        [HttpGet]
        public ViewResult GestionarCurso(string query = "")
        {
            var datos = icurso.ByQueryAll(query);
            return View("GestionarCurso",datos);
        }

        [HttpGet]
        public ViewResult CrearCurso()
        {
            return View("CrearCurso");
        }

        [HttpPost]
        public ActionResult CrearCurso(Curso curso)
        {
            EjecutarValidaciones(curso);

            if (ModelState.IsValid)
            {
                icurso.AddCurso(curso);
                TempData["UpdateSuccess"] = "Se Guardó Correctamente";

                return RedirectToAction("GestionarCurso");
            }
            return View("CrearCurso", curso);
        }

        [HttpGet]
        public ViewResult ActualizarCurso(int idcurso)
        {
            var data = icurso.FindCurso(idcurso);
            return View("ActualizarCurso", data);
        }

        [HttpPost]
        public ActionResult ActualizarCurso(Curso curso)
        {
            EjecutarValidaciones(curso);
            if (ModelState.IsValid)
            {
                icurso.UpdateCurso(curso);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("GestionarCurso");
            }
            var data = icurso.FindCurso(curso.IdCurso);
            return View("ActualizarCurso", data);
        }

        [HttpGet]
        public ActionResult Delete(int idCurso)
        {
            icurso.DeleteCurso(idCurso);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("GestionarCurso");
        }

        [HttpGet]
        public ViewResult DetalleCurso(int idCurso)
        {
            var data = icurso.FindCurso(idCurso);
            return View("DetalleCurso", data);
        }


        //Validaciones
        private void EjecutarValidaciones(Curso curso)
        {
            if (!CursoExiste(curso.NombreCurso, curso.IdCurso))
            {
                ModelState.AddModelError("NombreCurso", "El curso ya existe");
            }

            if (string.IsNullOrEmpty(curso.NombreCurso))
                ModelState.AddModelError("NombreCurso", "El nombre del Curso es obligatorio");
            if (curso.NotaMinima < 0 || curso.NotaMinima > 20)
                ModelState.AddModelError("NotaMinima", "Nota Mínima debe estar en el rango de 0 a 20 puntos");
        }

        private bool CursoExiste(string nombrecurso, int id)
        {
            var repo = new CursoR(new TBZContext());
            if (repo.CursoExiste(nombrecurso, id) > 0)
                return false;
            return true;
        }

    }
}
