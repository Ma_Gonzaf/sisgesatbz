﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZCurso
{
    public class Matricula
    {
        public int IdMatricula { get; set; }
        public DateTime FechaMatricula { get; set; }

        public Estudiante Estudiante { get; set; }
        public int IdEstudiante { get; set; }

        public Grado Grado { get; set; }
        public int IdGrado { get; set; }
    }
}
