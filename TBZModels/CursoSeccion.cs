﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZCurso
{
    public class CursoSeccion
    {
        public Seccion Seccion { get; set; }
        public int IdSeccion { get; set; }

        public Curso Curso { get; set; }
        public int IdCurso { get; set; }

        public Docente Docente { get; set; }
        public int IdDocente { get; set; }

        public DateTime FechaAsignacion { get; set; }
    }
}
