﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZCurso
{
    public class Apoderado
    {
        public int DniApoderado { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

        public string NombreCompleto
        {
            get
            {
                return ApellidoPaterno + " " + ApellidoMaterno + ", " + Nombres;
            }
        }
    }
}
