﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZCurso
{
    public class Seccion
    {
        public int IdSeccion { get; set; }
        public string NombreSeccion { get; set; }
        public int NroVacantes { get; set; }

        public Grado Grado { get; set; }
        public int IdGrado { get; set; }
        
        public ICollection<CursoSeccion> CursoSeccion { get; set; }
        public Seccion()
        {
            this.CursoSeccion = new HashSet<CursoSeccion>();
        }
    }
}
