﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TBZCurso;
using SISGEZATBZVisual.Repository.Mapping;

namespace TBZRepository
{
    public class TBZContext:DbContext
    {
        public TBZContext()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TBZContext>());
        }
      
        public DbSet<Apoderado> Apoderado { get; set; }
        public DbSet<Estudiante> Estudiante { get; set; }
        public DbSet<Matricula> Matricula { get; set; }
        public DbSet<Grado> Grado { get; set; }
        public DbSet<Seccion> Seccion { get; set; }
        public DbSet<Docente> Docente { get; set; }
        public DbSet<Curso> Curso { get; set; }
        public DbSet<CursoSeccion> CursoSeccion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApoderadoMap());
            modelBuilder.Configurations.Add(new EstudianteMap());
            modelBuilder.Configurations.Add(new MatriculaMap());
            modelBuilder.Configurations.Add(new GradoMap());
            modelBuilder.Configurations.Add(new SeccionMap());
            modelBuilder.Configurations.Add(new DocenteMap());
            modelBuilder.Configurations.Add(new CursoMap());
            modelBuilder.Configurations.Add(new CursoSeccionMap());
        }
    }
}
