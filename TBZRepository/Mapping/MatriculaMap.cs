﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class MatriculaMap:EntityTypeConfiguration<Matricula>
    {
        public MatriculaMap()
        {
            this.HasKey(c => c.IdMatricula);

            //propiedades

            this.Property(r => r.IdMatricula)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(r => r.FechaMatricula)
                .IsOptional();

            //table
            this.ToTable("Matricula");

            //Relaciones
            this.HasRequired(r => r.Estudiante)
                .WithMany()
                .HasForeignKey(r => r.IdEstudiante)
                .WillCascadeOnDelete(false);

            this.HasRequired(r => r.Grado)
                .WithMany()
                .HasForeignKey(r => r.IdGrado)
                .WillCascadeOnDelete(false);
        }
    }
}
