﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class CursoSeccionMap:EntityTypeConfiguration<CursoSeccion>
    {
        public CursoSeccionMap()
        {
            this.HasKey(c => new { c.IdSeccion,c.IdCurso });

            //table
            this.ToTable("CursoSeccion");
             
            this.HasRequired(c => c.Curso)
                   .WithMany()
                   .HasForeignKey(c => c.IdCurso)
                    .WillCascadeOnDelete(false);

            this.HasRequired(c => c.Seccion)
                   .WithMany(c => c.CursoSeccion)
                   .HasForeignKey(c => c.IdSeccion)
                    .WillCascadeOnDelete(false);
        }
    }
}
