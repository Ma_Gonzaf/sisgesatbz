﻿using TBZCurso;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class EstudianteMap:EntityTypeConfiguration<Estudiante>
    {
        public EstudianteMap()
        {
            this.HasKey(r => r.IdEstudiante);

            //propiedades

            this.Property(r => r.IdEstudiante)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(r => r.DniEstudiante)
                 .HasMaxLength(8)
                 .IsRequired();

            this.Property(r => r.Nombres)
                .HasMaxLength(80)
                .IsRequired();

            this.Property(r => r.ApellidoPaterno)
               .IsRequired()
               .HasMaxLength(80);

            this.Property(c => c.ApellidoMaterno)
               .HasMaxLength(80)
               .IsRequired();

            this.Property(c => c.Sexo)
                .HasMaxLength(10)
               .IsRequired();

             this.Property(c => c.Direccion)
            .HasMaxLength(80)
            .IsRequired();

             this.Property(c => c.Telefono)
                 .HasMaxLength(80)
                .IsRequired();

             this.Property(c => c.FechaNacimiento)
                .IsRequired();

            //table
            this.ToTable("Estudiante");

            //Relaciones
            this.HasRequired(r => r.Apoderado)
                .WithMany()
                .HasForeignKey(r => r.DniApoderado)
                .WillCascadeOnDelete(false);
        }
    }
}
