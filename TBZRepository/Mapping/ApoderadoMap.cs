﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class ApoderadoMap:EntityTypeConfiguration<Apoderado>
    {
        public ApoderadoMap()
        {
            this.HasKey(r => r.DniApoderado);

            //propiedades

            this.Property(r => r.DniApoderado)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(r => r.Nombres)
                .HasMaxLength(80)
                .IsRequired();

            this.Property(r => r.ApellidoPaterno)
               .IsRequired()
               .HasMaxLength(80);

            this.Property(c => c.ApellidoMaterno)
               .HasMaxLength(80)
               .IsRequired();

            this.Property(c => c.Sexo)
                .HasMaxLength(10)
               .IsRequired();

            this.Property(c => c.Direccion)
                  .HasMaxLength(80)
                   .IsRequired();

            this.Property(c => c.Telefono)
                  .HasMaxLength(80)
               .IsRequired();

            //table
            this.ToTable("Apoderado");
        }
    }
}
