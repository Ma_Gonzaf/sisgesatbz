﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using TBZCurso;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class SeccionMap:EntityTypeConfiguration<Seccion>
    {
        public SeccionMap()
        {
            this.HasKey(c => c.IdSeccion);

            //propiedades

            this.Property(r => r.IdSeccion)
               .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(c => c.NombreSeccion)
            .HasMaxLength(80)
            .IsRequired();

            this.Property(c => c.NroVacantes)
            .IsRequired();

            //table
            this.ToTable("Seccion");

            //Relaciones
            this.HasRequired(r => r.Grado)
                .WithMany()
                .HasForeignKey(r => r.IdGrado)
                .WillCascadeOnDelete(false);
        }
    }
}
