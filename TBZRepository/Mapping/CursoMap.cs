﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class CursoMap:EntityTypeConfiguration<Curso>
    {
        public CursoMap()
        {
            this.HasKey(r => r.IdCurso);

            //propiedades
            this.Property(r => r.IdCurso)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(r => r.NombreCurso)
                .HasMaxLength(80)
                .IsRequired();

            this.Property(r => r.NotaMinima)
               .IsOptional();

            //table
            this.ToTable("Curso");
        }
    }
}
