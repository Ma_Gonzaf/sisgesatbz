﻿using TBZCurso;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISGEZATBZVisual.Repository.Mapping
{
    public class GradoMap:EntityTypeConfiguration<Grado>
    {
        public GradoMap()
        {
            this.HasKey(c => c.IdGrado);

            //propiedades
               this.Property(r => r.IdGrado)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
           
            this.Property(r => r.NombreGrado)
               .IsRequired()
               .HasMaxLength(80);

            //table
            this.ToTable("Grado");
        }
    }
}
