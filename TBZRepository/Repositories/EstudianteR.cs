﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;
using TBZRepository;
using TBZInterfaces;
using TBZ.Interfaces;
using System.Data.Entity;

namespace TBZ.Repository.Repositories
{
    public class EstudianteR:IEstudiante
    {
        TBZContext context;

        public EstudianteR(TBZContext context)
        {
            this.context = context;
        }

        public List<Estudiante> AllEstudiante()
        {
            var result = from p in context.Estudiante select p;
            return result.ToList();
        }

        public List<Estudiante> ByQueryAll(string query)
        {
            var dbQuery = (from p in context.Estudiante.Include("Apoderado") select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombres.Contains(query) || o.ApellidoPaterno.Contains(query) || o.ApellidoMaterno.Contains(query) || o.Apoderado.Nombres.Contains(query) || o.Apoderado.ApellidoPaterno.Contains(query) || o.Apoderado.ApellidoMaterno.Contains(query));

            return dbQuery.ToList();
        }

        public void AddEstudiante(Estudiante estudiante)
        {
            context.Estudiante.Add(estudiante);
            context.SaveChanges();
        }

        public Estudiante FindEstudiante(int IdEstudiante)
        {
            var result = from p in context.Estudiante.Include("Apoderado") where p.IdEstudiante == IdEstudiante select p;
            return result.FirstOrDefault();
        }

        public void UpdateEstudiante(Estudiante estudiante)
        {
            context.Entry(estudiante).State = EntityState.Modified;
            context.SaveChanges();
        }

        public int DniExiste(string dniestudiante, int idestudiante)
        {
            return (from p in context.Estudiante where p.DniEstudiante == dniestudiante && p.IdEstudiante != idestudiante select p).Count();
        }
    }
}
