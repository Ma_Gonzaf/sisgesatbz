﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZ.Interfaces;
using TBZRepository;

namespace TBZ.Repository.Repositories
{
    public class ApoderadoR:IApoderadoR
    {
        TBZContext context;

        public ApoderadoR(TBZContext context)
        {
            this.context = context;
        }

        public int DniExisteApoderado(string dniApoderado)
        {
            return (from p in context.Apoderado where p.DniApoderado.Equals(dniApoderado) select p).Count();

        }
    }
}
