﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZInterfaces;

namespace TBZRepository.Repositories
{
    public class CursoR:ICurso
    {
        TBZContext context;

        public CursoR(TBZContext context)
        {
            this.context = context;
        }

        public List<TBZCurso.Curso> AllCurso()
        {
            var result = from p in context.Curso select p;
            return result.ToList();
        }

        public List<TBZCurso.Curso> ByQueryAll(string query)
        {
            var dbQuery = (from p in context.Curso select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.NombreCurso.Contains(query));

            return dbQuery.ToList();
        }

        public void AddCurso(TBZCurso.Curso curso)
        {
            context.Curso.Add(curso);
            context.SaveChanges();
        }

        public TBZCurso.Curso FindCurso(int idCurso)
        {
            var result = from p in context.Curso where p.IdCurso == idCurso select p; ;
            return result.FirstOrDefault();
        }

        public void UpdateCurso(TBZCurso.Curso curso)
        {
            context.Entry(curso).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void DeleteCurso(int idCurso)
        {
            var existe = context.Curso.Find(idCurso);

            if (existe != null)
            {
                context.Curso.Remove(existe);
                context.SaveChanges();
            }
        }

        public int CursoExiste(string nombrecurso, int id)
        {
            return (from p in context.Curso where p.NombreCurso  == nombrecurso && p.IdCurso != id select p).Count();

        }
    }
}