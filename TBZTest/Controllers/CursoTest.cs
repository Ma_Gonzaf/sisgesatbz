﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using TBZInterfaces;
using SISGESATBZ1.Controllers;
using TBZCurso;
using System.Web.Mvc;

namespace TBZ.Test.Controllers
{
    [TestFixture]
    public class CursoTest
    {
        [Test]
        public void TestGestionarEstudianteReturnView()
        {
            var mock = new Mock<ICurso>();
            var query = "";
            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<Curso>());
            var controller = new CursosController(mock.Object);
            var view = controller.GestionarCurso(query);

            mock.Verify(o => o.ByQueryAll(query), Times.Once);
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("GestionarCurso", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Curso>), view.Model);
        }

        [Test]
        public void TestIndexCallAllMethodFromRepository()
        {
            var mock = new Mock<ICurso>();
            mock.Setup(o => o.AllCurso()).Returns(new List<Curso>());
            var controller = new CursosController(mock.Object);
            controller.GestionarCurso();
            mock.Verify(o => o.AllCurso(), Times.Exactly(0));
        }

        [Test]
        public void TestCreateReturnView()
        {
            var repositoryMock = new Mock<ICurso>();
            var controler = new CursosController(repositoryMock.Object);
            var view = controler.CrearCurso();
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("CrearCurso", view.ViewName);
        }

       
        [Test]
        public void TestEditReturnView()
        {
            var curso = new Curso();
            var repositoryMock = new Mock<ICurso>();
            repositoryMock.Setup(o => o.FindCurso(curso.IdCurso)).Returns(new Curso());
            var controller = new CursosController(repositoryMock.Object);
            var view = controller.ActualizarCurso(curso.IdCurso);
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("ActualizarCurso", view.ViewName);
        }

        [Test]
        public void TestDeleteReturnRouteResult()
        {
            var curso = new Curso();
            var repositoryMock = new Mock<ICurso>();
            repositoryMock.Setup(o => o.FindCurso(curso.IdCurso)).Returns(new Curso());
            var controller = new CursosController(repositoryMock.Object);
            var view = controller.Delete(curso.IdCurso);
            Assert.IsInstanceOf(typeof(RedirectToRouteResult), view);
        }

        [Test]
        public void TestDetailsReturnView()
        {
            var curso = new Curso();
            var repositoryMock = new Mock<ICurso>();
            repositoryMock.Setup(o => o.FindCurso(curso.IdCurso)).Returns(new Curso());
            var controller = new CursosController(repositoryMock.Object);
            var view = controller.DetalleCurso(curso.IdCurso);
            Assert.AreEqual("DetalleCurso", view.ViewName);
        }

    }
}
