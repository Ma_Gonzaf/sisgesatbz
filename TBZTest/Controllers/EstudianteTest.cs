﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TBZInterfaces;
using SISGESATBZ1.Controllers;
using TBZCurso;
using System.Web.Mvc;
using TBZ.Interfaces;

namespace TBZ.Test.Controllers
{
    [TestFixture]
    
    public class EstudianteTest
    {
        [Test]
        public void TestGestionarEstudianteReturnView()
        {
            var mock = new Mock<IEstudiante>();
            var query = "";
            mock.Setup(o => o.ByQueryAll(query)).Returns(new List<Estudiante>());
            var controller = new EstudiantesController(mock.Object);
            var view = controller.GestionarEstudiante(query);

            mock.Verify(o => o.ByQueryAll(query), Times.Once);
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("GestionarEstudiante", view.ViewName);
            Assert.IsInstanceOf(typeof(List<Estudiante>), view.Model);
        }

        [Test]
        public void TestIndexCallAllMethodFromRepository()//este metodo no utilizo en el index
        {
            var mock = new Mock<IEstudiante>();
            mock.Setup(o => o.AllEstudiante()).Returns(new List<Estudiante>());
            var controller = new EstudiantesController(mock.Object);
            controller.GestionarEstudiante();
            mock.Verify(o => o.AllEstudiante(), Times.Exactly(0));
        }

        [Test]
        public void TestCreateReturnView()
        {
            var repositoryMock = new Mock<IEstudiante>();
            var controler = new EstudiantesController(repositoryMock.Object);
            var view = controler.CrearEstudiante();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("CrearEstudiante", view.ViewName);
        }

        [Test]
        public void TestEditReturnView()
        {
            var estudiante = new Estudiante();
            var repositoryMock = new Mock<IEstudiante>();
            repositoryMock.Setup(o => o.FindEstudiante(estudiante.IdEstudiante)).Returns(new Estudiante());
            var controller = new EstudiantesController(repositoryMock.Object);
            var view = controller.ActualizarEstudiante(estudiante.IdEstudiante);

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("ActualizarEstudiante", view.ViewName);
        }

        [Test]
        public void TestDetailsReturnView()
        {
            var estudiante = new Estudiante();

            var repositoryMock = new Mock<IEstudiante>();
            repositoryMock.Setup(o => o.FindEstudiante(estudiante.IdEstudiante)).Returns(new Estudiante());

            var controller = new EstudiantesController(repositoryMock.Object);

            var view = controller.DetalleEstudiante(estudiante.IdEstudiante);

            Assert.AreEqual("DetalleEstudiante", view.ViewName);
        }

    }
}
