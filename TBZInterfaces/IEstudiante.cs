﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZCurso;

namespace TBZ.Interfaces
{
    public interface IEstudiante
    {
        List<Estudiante> AllEstudiante();

        List<Estudiante> ByQueryAll(string query);

        void AddEstudiante(Estudiante estudiante);

        Estudiante FindEstudiante(int IdEstudiante);

        void UpdateEstudiante(Estudiante estudiante);

        int DniExiste(string dniestudiante, int idestudiante);
        
    }
}
