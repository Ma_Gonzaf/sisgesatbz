﻿using TBZCurso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZInterfaces
{
    public interface ICurso
    {
        List<Curso> AllCurso();

        List<Curso> ByQueryAll(string query);

        void AddCurso(Curso curso);

        Curso FindCurso(int idCurso);

        void UpdateCurso(Curso curso);

        void DeleteCurso(int idCurso);
    }
}
