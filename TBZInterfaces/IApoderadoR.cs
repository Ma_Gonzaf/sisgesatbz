﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBZ.Interfaces
{
    public interface IApoderadoR
    {
        int DniExisteApoderado(string dniApoderado);

    }
}
