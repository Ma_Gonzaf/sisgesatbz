﻿using TBZCurso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBZRepository;

namespace GenerateDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var curso = new Curso()
            {
                NombreCurso = "Matematica1",
                NotaMinima = 2

            };

            var curso01 = new Curso()
            {
                NombreCurso = "Matematica2",
                NotaMinima = 1
            };

            var context = new TBZContext();
            Console.WriteLine("Creando Base de Datos");

            context.Curso.Add(curso);
            context.Curso.Add(curso01);


            context.SaveChanges();

            Console.WriteLine("Base de datos Creada OK!!");
            Console.ReadLine();
        }
    }
}
